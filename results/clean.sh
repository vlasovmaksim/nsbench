#!/bin/bash

echo "Cleaning all results (vtu and pvd files). Press any key to continue."
read

rm -f *.vtu *.pvd *.xml
