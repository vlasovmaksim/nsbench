# README #

The project is a fork of the NSbench project.

NSbench is a collection of Python scripts based on FEniCS/DOLFIN for benchmarking a number of common methods for solving the incompressible Navier-Stokes equations.

Link to the original project:
https://launchpad.net/nsbench

### How do I get set up? ###

* Download and run.